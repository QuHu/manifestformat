/* jslint node:true */
/* global describe:true */
/* global it:true */

'use strict';

const expect = require('expect.js'),
    manifestFormat = require('../manifestformat.js');

describe('parseManifest', function () {
    it('errors for empty string', function () {
        expect(manifestFormat.parseString('').error).to.be.an(Error);
    });

    it('errors for invalid json', function () {
        expect(manifestFormat.parseString('garbage').error).to.be.an(Error);
    });

    const manifest = {
        id: 'io.cloudron.test',
        title: 'Bar App',
        description: 'Long long ago, there was foo',
        tagline: 'Not your usual Foo App',
        author: 'Herbert Burgermeister',
        manifestVersion: 1,
        version: '0.1.2',
        dockerImage: 'girish/foo:0.2',
        httpPort: 23,
        website: 'https://example.com',
        contactEmail: 'support@example.com'
    };

    manifestFormat.SCHEMA_V1.required.forEach(function (key) {
        const manifestCopy = Object.assign({ }, manifest);
        delete manifestCopy[key];
        it('errors for missing ' + key, function () {
            expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
        });
    });

    new Array(null, [ 23 ], [ 'mysql', 34 ], [ null, 'mysql' ]).forEach(function (invalidAddon, idx) {
        it('fails for invalid addon testcase ' + idx, function () {
            const manifestCopy = Object.assign({ }, manifest);
            manifestCopy.addons = invalidAddon;
            expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
        });
    });

    it('fails for bad version', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.version = '0.2';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad id', function () {
        const manifestCopy = Object.assign({ }, manifest);
        ['simply', '12de', 'in..x', 'x.com', 'no.hy-phen' ].forEach(function(badId) {
            manifestCopy.id = badId;
            expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
        });
    });

    it('fails for bad minBoxVersion', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.minBoxVersion = '0.2';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad maxBoxVersion', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.maxBoxVersion = '0.2';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad targetBoxVersion', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.targetBoxVersion = '0.2';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad manifestVersion', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.manifestVersion = 3;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad capabilities', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.capabilities = [ 'test' ];

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad changelog', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.changelog = 34;

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.changelog = [ ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.changelog = [ null ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.changelog = [ 56 ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad tcpPorts', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.tcpPorts = 45;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.tcpPorts = { 'ENV$': { } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad env

        manifestCopy.tcpPorts = { 'smallercase': { } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad env

        manifestCopy.tcpPorts = { 'CLOUDRON_OOPS': { } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad env

        manifestCopy.tcpPorts = { 'PORT_NAME': { } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // missing description

        manifestCopy.tcpPorts = { 'PORT_NAME': { description: 34 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad description

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 34 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad title

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 't', description: 'long enough' } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // short title

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', containerPort: 'invalid' } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad containerPort

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', containerPort: NaN } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad containerPort

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', containerPort: -2 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad containerPort

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 'invalid' } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad defaultValue

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: NaN } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad defaultValue

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: -2 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad defaultValue

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 4000, portCount: 0 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad portCount

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 4000, portCount: '100' } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad portCount

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 4000, portCount: NaN } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad portCount

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 4000, portCount: -1 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad portCount

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 4000, portCount: 1001 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad portCount

        manifestCopy.tcpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 4000, portCount: 1001 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad portCount
    });

    it('fails for bad udpPorts', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.udpPorts = 45;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.udpPorts = { 'ENV$': { } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad env

        manifestCopy.udpPorts = { 'smallercase': { } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad env

        manifestCopy.udpPorts = { 'PORT_NAME': { } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // missing description

        manifestCopy.udpPorts = { 'PORT_NAME': { description: 34 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad description

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 34 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad title

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 't', description: 'long enough' } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // short title

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 'title', description: 'description', containerPort: 'invalid' } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad containerPort

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 'title', description: 'description', containerPort: NaN } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad containerPort

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 'title', description: 'description', containerPort: -2 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad containerPort

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 'invalid' } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad defaultValue

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: NaN } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad defaultValue

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: -2 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad defaultValue

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 4000, portCount: 0 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad portCount

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 4000, portCount: '100' } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad portCount

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 4000, portCount: NaN } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad portCount

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 4000, portCount: -1 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad portCount

        manifestCopy.udpPorts = { 'PORT_NAME': { title: 'title', description: 'description', defaultValue: 4000, portCount: 1001 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad portCount
    });

    it('fails for duplicate key in udp/tcpPorts', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.udpPorts = { 'UGOOD_PORT': { title: 't1', description: 'description'}, 'DUP_PORT': { title: 't2', description: 'duplicate' } };
        manifestCopy.tcpPorts = { 'DUP_PORT': { title: 't2', description: 'duplicate' }, 'TGOOD_PORT': { title: 't1', description: 'description'} };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // duplicate DUP_PORT
    });

    it('fails for bad addon', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.addons = { 'andy': { } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad tags', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.tags = [ 234 ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        const manifestCopy2 = Object.assign({ }, manifest);
        manifestCopy2.tags = [ 'again', 'and', 'again' ]; // duplicate
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy2)).error).to.be.an(Error);
    });

    it('fails for bad scheduler addon', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.addons = {
            scheduler: {
                'nice': 'try'
            }
        };

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.addons = {
            scheduler: {
                'task1': { schedule: '1 2 3 4 m', command: 'blah' }
            }
        };

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeeds for good scheduler pattern', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.addons = {
            'scheduler': {
                'task1': { schedule: '* * * * *', command: 'blah' }
            }
        };

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds for good scheduler named pattern', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.addons = {
            'scheduler': {
                'task1': { schedule: '@service', command: 'blah' }
            }
        };

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds for good capabilities', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.capabilities = [ ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);

        const manifestCopy2 = Object.assign({ }, manifest);
        manifestCopy2.capabilities = [ 'net_admin', 'mlock', 'ping' ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy2)).manifest).to.eql(manifestCopy2);
    });

    it('succeeds for good tcpPorts', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.tcpPorts = { 'PORT_NAME': { title: '12345', description: '12345', containerPort: 546 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);

        const manifestCopy2 = Object.assign({ }, manifest);
        manifestCopy2.tcpPorts = { 'PORT_NAME': { title: '12345', description: '12345', containerPort: 65535 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy2)).manifest).to.eql(manifestCopy2);

        const manifestCopy3 = Object.assign({ }, manifest);
        manifestCopy3.tcpPorts = { 'PORT_NAME': { title: '12345', description: '12345', containerPort: 65535, portCount: 100 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy3)).manifest).to.eql(manifestCopy3);
    });

    it('succeeds for good udpPorts', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.udpPorts = { 'PORT_NAME': { title: '12345', description: '12345', containerPort: 546 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);

        const manifestCopy2 = Object.assign({ }, manifest);
        manifestCopy2.udpPorts = { 'PORT_NAME': { title: '12345', description: '12345', containerPort: 65535 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy2)).manifest).to.eql(manifestCopy2);

        const manifestCopy3 = Object.assign({ }, manifest);
        manifestCopy3.udpPorts = { 'PORT_NAME': { title: '12345', description: '12345', containerPort: 65535, portCount: 100 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy3)).manifest).to.eql(manifestCopy3);
    });

    it('succeeds for minimal valid manifest', function () {
        expect(manifestFormat.parseString(JSON.stringify(manifest)).manifest).to.eql(manifest);
    });

    it('succeeds for maximal valid manifest', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.minBoxVersion = '0.0.1';
        manifestCopy.maxBoxVersion = '1.0.0';
        manifestCopy.targetBoxVersion = '1.0.0';
        manifestCopy.addons = {
            'mysql': { },
            'postgresql': { },
            'mongodb': { cacheSize: 34 }
        };
        manifestCopy.changelog = 'Change upload size limit';
        manifestCopy.tags = [ 'wiki', 'collaboration' ];
        manifestCopy.capabilities = [ 'net_admin' ];

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('fails for bad memoryLimit', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.memoryLimit = '1p';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeeds for int memoryLimit', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.memoryLimit = 123456;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds for string memoryLimit', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.memoryLimit = '10MB';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });


    it('fails for non uri type mediaLinks', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.mediaLinks = [ 'http://foobar.de', 'http://baz', 'foobar' ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for duplicate mediaLinks', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.mediaLinks = [ 'http://foobar.de', 'http://baz.com', 'http://foobar.de' ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeeds with mediaLinks', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.mediaLinks = [ 'http://foobar.de', 'http://baz.com' ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds with upstreamVersion', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.upstreamVersion = '1.2';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds with postInstallMessage', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.postInstallMessage = 'Can you see this? Thanks for installing';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds with optionalSso', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.optionalSso = true;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('fails with invalid documentationUrl', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.documentationUrl = 'not-valid';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeeds with forumUrl', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.forumUrl = 'https://cloudron.io/documentation/apps/wordpress';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('fails with invalid forumUrl', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.forumUrl = 'not-valid';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeeds with documentationUrl', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.documentationUrl = 'https://cloudron.io/documentation/apps/wordpress';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds for proxyAuth', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.addons = { proxyAuth: {} };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    // deprecated
    it('fails for bad multiDomain', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.multiDomain = { };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    // deprecated
    it('succeeds for multiDomain', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.multiDomain = true;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('fails for bad aliasableDomain', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.aliasableDomain = { };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeeds for aliasableDomain', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.aliasableDomain = true;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('fails with no httpPort or httpPorts', function () {
        const manifestCopy = Object.assign({ }, manifest);
        delete manifestCopy.httpPort;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeed with httpPorts', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.httpPorts = { 'UI_DOMAIN': { title: 'title', description: 'description', containerPort: 9000, defaultValue: 'ui.domain', aliasableDomain: true } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeed with logPaths', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.logPaths = [ 'path1', 'path2' ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('fails with bad runtimeDirs', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.runtimeDirs = [ '/whatever' ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeed with runtimeDirs', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.runtimeDirs = [ '/app/code', '/app/code/bar', '/home/cloudron/.npm' ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('requires loginRedirectUri for oidc', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.addons = { oidc: {} };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.addons = { oidc: { loginRedirectUri: '/someUri' } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);

        manifestCopy.addons = { oidc: { loginRedirectUri: '/someUri', logoutRedirectUri: '' } }; // extra properties test
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('fails for bad checklist', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.checklist = 'a toplevel string it must not be';

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.checklist = {
            'task1': 'an object it must be'
        };

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.checklist = {
            'task1': { info: 'requires a message property' }
        };

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.checklist = {
            'task1': { sso: 123, message: 'sso if present must be a boolean' }
        };

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeeds for good checklist', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.checklist = {
            'task1': { message: 'really has to be done' },
            '00-first-this': { sso: false, message: 'totally valid' },
            'that': { sso: true, message: 'also totally valid' }
        };

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });
});

describe('isId', function () {
    it('return false for invalid ids', function () {
        expect(manifestFormat.isId('combarzoo')).to.be(false);
        expect(manifestFormat.isId('com.bar-zoo')).to.be(false);
        expect(manifestFormat.isId('c.bar.zoo')).to.be(false);
    });

    it('succeeds for valid ids', function () {
        expect(manifestFormat.isId('com.bar.zoo')).to.be(true);
        expect(manifestFormat.isId('com.bar_zoo')).to.be(true);
        expect(manifestFormat.isId('com.barzoo')).to.be(true);
        expect(manifestFormat.isId('co.bar.zoo')).to.be(true);
    });
});

describe('checkAppstoreRequirements', function () {
    const manifest = {
        id: 'io.cloudron.test',
        title: 'Bar App',
        description: 'Long long ago, there was foo',
        tagline: 'Not your usual Foo App',
        author: 'Herbert Burgermeister',
        manifestVersion: 1,
        version: '0.1.2',
        dockerImage: 'cloudron/io.cloudron.test:0.1.2',
        httpPort: 23,
        healthCheckPath: '/',
        website: 'https://example.com',
        contactEmail: 'support@example.com',
        tags: [ 'something' ],
        mediaLinks: [ 'https://foo.ong' ],
        icon: 'file://icon.png',
        changelog: 'file://changelog.md',
        customAuth: false,
        postInstallMessage: 'Nice!',
    };

    it('fails without tags', function () {
        const manifestCopy = Object.assign({ }, manifest);
        delete manifestCopy.tags;
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('fails without mediaLinks', function () {
        const manifestCopy = Object.assign({ }, manifest);
        delete manifestCopy.mediaLinks;
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('fails without icon', function () {
        const manifestCopy = Object.assign({ }, manifest);
        delete manifestCopy.icon;
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('fails without changelog', function () {
        const manifestCopy = Object.assign({ }, manifest);
        delete manifestCopy.changelog;
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('fails with prerelease version', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.version = '1.2.3-pre';
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('fails with build version', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.version = '1.2.3+build';
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('succeeds with numeric prerelease version', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.dockerImage = 'cloudron/io.cloudron.test:1.2.3-1-timestamp';
        manifestCopy.version = '1.2.3-1';
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be(null);
    });

    it('requires 7.1 atleast for upstreamVersion', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.upstreamVersion = '3.5';
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
        manifestCopy.minBoxVersion = '7.1.0';
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be(null);
    });

    it('requires 7.1 atleast for logPaths', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.logPaths = [ 'somefile' ];
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
        manifestCopy.minBoxVersion = '7.1.0';
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be(null);
    });

    it('requires 7.7 atleast for tcpPorts.portCount', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.tcpPorts = { 'env': { title: 'title', description: 'description', defaultValue: 4000, portCount: 100 } };
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
        manifestCopy.minBoxVersion = '7.7.0';
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be(null);
    });

    it('requires 7.7 atleast for udpPorts.portCount', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.udpPorts = { 'env': { title: 'title', description: 'description', defaultValue: 4000, portCount: 100 } };
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
        manifestCopy.minBoxVersion = '7.7.0';
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be(null);
    });

    it('requires 8.0.0 atleast for checklist', function () {
        const manifestCopy = Object.assign({ }, manifest);
        manifestCopy.checklist = { 'task': { sso: true, message: 'do this and that' } };
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
        manifestCopy.minBoxVersion = '8.0.0';
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be(null);
    });

    it('succeeds with all requirements', function () {
        const manifestCopy = Object.assign({ }, manifest);
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be(null);
    });
});

